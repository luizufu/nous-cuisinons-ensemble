#include "window_configuration.h"

const char *kFlutterWindowTitle = "nous_cuisinons_ensemble";
const unsigned int kFlutterWindowWidth = 800;
const unsigned int kFlutterWindowHeight = 600;
